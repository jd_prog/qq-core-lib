import {Component, Inject, OnInit} from "@angular/core";
import {MAT_SNACK_BAR_DATA} from "@angular/material";

@Component({
  selector: 'snack-bar-component',
  templateUrl: 'snack.bar.component.html',
})
export class SnackBarComponent implements OnInit {
  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: any) {
  }

  ngOnInit() {
  }
}
