import {NgModule} from '@angular/core';
import {ConfirmDialogWindowComponent} from './confirm-dialog-window/confirm-dialog-window';
import {MatButtonModule, MatDialogModule} from '@angular/material';
import {SnackBarComponent} from "./snack-bar-component/snack.bar.component";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule

  ],
  declarations: [
    ConfirmDialogWindowComponent,
    SnackBarComponent
  ],
  entryComponents: [
    ConfirmDialogWindowComponent,
    SnackBarComponent

  ],
  exports: [
    ConfirmDialogWindowComponent,
    SnackBarComponent
  ]
})
export class ComponentsModule {
}
