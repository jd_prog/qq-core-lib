import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'qsl-confirm-dialog-window',
  templateUrl: './confirm.dialog.window.html',
  styleUrls: ['./confirm.dialog.window.less']

})
export class ConfirmDialogWindowComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogWindowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {

  }

  onNegativeAnswerClick() {
    this.dialogRef.close(false);
  }

  onPositiveAnswerClick() {
    this.dialogRef.close(true);
  }

}
