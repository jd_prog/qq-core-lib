import {Topic} from '../../topic/model/Topic';

export class Stream {

  constructor( public id: number,
               public location: string,
               public name: string,
               public topics: Topic[]) {}
}
