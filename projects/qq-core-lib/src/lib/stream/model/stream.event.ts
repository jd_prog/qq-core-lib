import {Stream} from './stream';
import {StreamEventType} from './stream.event.type';


export class StreamEvent {
  public eventType: StreamEventType;
  public stream: Stream;

  constructor(event_: StreamEventType, stream_: Stream ) {
    this.eventType = event_;
    this.stream = stream_;
  }

}
