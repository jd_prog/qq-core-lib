import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {StreamEvent} from '../model/stream.event';
import {Stream} from '../model/stream';
import {StreamEventType} from '../model/stream.event.type';

@Injectable({
  providedIn: 'root'
})
export class StreamEventService {

  constructor() {}

  private subject: Subject<StreamEvent> = new Subject<StreamEvent>();

  addEvent(stream: Stream) {
    this.subject.next(new StreamEvent(StreamEventType.add , stream));
  }

  deleteEvent(stream: Stream) {
    this.subject.next( new StreamEvent( StreamEventType.delete, stream));
  }

  getEvent() {
        return this.subject;
  }

}
