import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Stream} from '../../model/stream';

@Injectable({
  providedIn: 'root'
})
export class StreamCreateService {

  subject = new Subject<Object>();

  cancleAdd() {
    this.subject.next({action: false, stream: '' });
  }

  AddStream(stream: Stream) {
    this.subject.next({action: true, stream: stream});
  }

}
