import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {Stream} from '../model/stream';
import {Observable} from 'rxjs';
import 'rxjs-compat/add/observable/empty';
import 'rxjs-compat/add/observable/from';




@Injectable({
  providedIn: 'root'
})
export class StreamService {

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {
  }

  getStreamById(streamId: string): Observable<Stream> {
    return this.http.get<Stream>(`${this.urlProvider.getUrl()}api/streams/${streamId}`);
  }

  getStreamsByEvent(eventId: string) {
    return this.http.get<Stream[]>(`${this.urlProvider.getUrl()}api/events/${eventId}/streams`);
  }

  add(eventId: number , stream: Stream) {
    return this.http.post<Stream>(`${this.urlProvider.getUrl()}api/events/${eventId}/streams/`, stream);
  }

  update(stream: Stream) {
    return this.http.put<Stream>(`${this.urlProvider.getUrl()}api/streams/`, stream);
  }

   delete(stream: Stream) {
     return this.http.delete<void>(`${this.urlProvider.getUrl()}api/streams/${stream.id}`);
   }
}
