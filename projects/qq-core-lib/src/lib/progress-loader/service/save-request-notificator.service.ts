import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class SaveRequestNotificator {

  private requestSubject = new Subject<boolean>();
  requestLifecycle: Observable<boolean> = this.requestSubject.asObservable();

  constructor() {
  }

  notify(show: boolean) {
    this.requestSubject.next(show);
  }
}
