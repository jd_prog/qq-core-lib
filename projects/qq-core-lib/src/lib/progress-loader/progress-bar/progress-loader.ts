import {Component, OnInit} from '@angular/core';
import {SaveRequestNotificator} from '../service/save-request-notificator.service';

@Component({
  selector: 'qsl-progress-loader',
  templateUrl: './progress-loader.html',
  styleUrls: ['./progress-loader.less']

})
export class ProgressLoaderComponent implements OnInit {
  showSpiner = false;
  showSuccess = false;
  showError = false;
  startSaveProcess = false;
  count = 0;

  constructor(private requestNotificator: SaveRequestNotificator) {
  }

  ngOnInit() {
    this.requestNotificator.requestLifecycle.subscribe(startSaveProcess => {
      this.startSaveProcess = startSaveProcess;
      this.turnOnSaveIndicator();
    });
  }

  zeroingCountOfRequest() {
    if (this.startSaveProcess) {
      this.count++;
    } else {
      if (!this.startSaveProcess) {
        --this.count;
      }
    }
  }

  setIndicators() {
    if (this.count !== 0) {
      this.showSpiner = true;
      this.showSuccess = false;
      this.showError = false;
    } else {
      if (this.count === 0) {
        setTimeout(() => {
          this.showSpiner = false;
          this.showError = false;
          this.showSuccess = true;
          setTimeout(() => {
            this.showSuccess = false;
          }, 1000);
        }, 1000);
      }
    }
  }

  errorIndicator() {
    this.count--;
    this.showSpiner = true;
    this.showSuccess = false;
    this.showError = false;
    setTimeout(() => {
      this.showSpiner = false;
      this.showError = true;
      setTimeout(() => {
        this.showError = false;
      }, 1000);
    }, 2000);
  }

  turnOnSaveIndicator() {
    if (this.startSaveProcess != null) {
      this.zeroingCountOfRequest();
      this.setIndicators();
    } else {
      this.errorIndicator();
    }
  }
}

