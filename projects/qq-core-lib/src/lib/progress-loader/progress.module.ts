import { NgModule } from '@angular/core';
import {ProgressLoaderComponent} from './progress-bar/progress-loader';
import {MatIconModule, MatProgressSpinnerModule} from '@angular/material';

@NgModule({
  imports: [
    MatIconModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    ProgressLoaderComponent
  ],
  exports: [
    ProgressLoaderComponent
  ]
})
export class ProgressModule { }
