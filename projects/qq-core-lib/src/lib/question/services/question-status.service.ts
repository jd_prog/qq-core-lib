import {Injectable} from '@angular/core';
import {Question} from '../models/Question';

@Injectable({
  providedIn: 'root'
})
export class QuestionStatusService {

  constructor() {}

  isUpcoming(isTopicPassed: boolean, question: Question): boolean {
    return isTopicPassed
      ? question.questionStatus === 'NEW' || question.questionStatus === 'ACTIVE'
      : question.questionStatus === 'NEW';
  }

}
