import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Question} from '../models/Question';
import {Observable} from 'rxjs';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {tap} from 'rxjs/operators';
import {BaseBehaviorSubject} from '../../event/services/base-behaviour.subject';

/**
 * Created by yana on 26.04.18.
 */

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  questionsCollection: BaseBehaviorSubject<Question> = new BaseBehaviorSubject<Question>();

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {
  }

  findAllByTopicId(topicId: number): Observable<Array<Question>> {
    return this.http.get<Array<Question>>(`${this.urlProvider.getUrl()}api/topics/${topicId}/questions`)
      .pipe(tap(questions => this.questionsCollection.replaceAll(questions)));
  }

  findById(questionId: number): Observable<Question> {
    return this.http.get<Question>(`${this.urlProvider.getUrl()}api/questions/${questionId}`);
  }

  post(topicId: string, question: Question): Observable<Question> {
    return this.http.post<Question>(`${this.urlProvider.getUrl()}api/topics/${topicId}/questions`, question);
  }

  vote(questionId: number): Observable<Question> {
    return this.http.post<Question>(`${this.urlProvider.getUrl()}api/questions/${questionId}/vote`, null);
  }

  removeVote(questionId: number): Observable<Question> {
    return this.http.post<Question>(`${this.urlProvider.getUrl()}api/questions/${questionId}/unvote`, null);
  }

  delete(questionId: number): Observable<void> {
    return this.http.delete<void>(`${this.urlProvider.getUrl()}api/questions/${questionId}`);
  }

  open(questionId: number): Observable<Question> {
    return this.http.post<Question>(`${this.urlProvider.getUrl()}api/questions/${questionId}/open`, null);
  }

  close(questionId: number): Observable<Question> {
    return this.http.post<Question>(`${this.urlProvider.getUrl()}api/questions/${questionId}/close`, null);
  }

}
