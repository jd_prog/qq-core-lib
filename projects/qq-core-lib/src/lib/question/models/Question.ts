import {Vote} from './Vote';
/**
 * Created by yana on 26.04.18.
 */
export class Question {

  id: number;
  votes: Vote[];
  content: string;
  questionStatus: string;
  authorDisplayName: string;
  authorUserName: string;
}
