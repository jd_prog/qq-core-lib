import { Injectable } from '@angular/core';
import moment from 'moment';
import {Topic} from '../model/Topic';

@Injectable({
  providedIn: 'root'
})
export class TopicStatusService {

  constructor() { }

  ongoing(topic: Topic): boolean {
    return moment(topic.startDate).isSameOrBefore(moment()) && moment(topic.endDate).isSameOrAfter(moment());
  }

  upcoming(topic: Topic): boolean {
    return moment(topic.startDate).isAfter(moment());
  }

  passed(topic: Topic): boolean {
    return moment(topic.endDate).isBefore(moment());
  }
}
