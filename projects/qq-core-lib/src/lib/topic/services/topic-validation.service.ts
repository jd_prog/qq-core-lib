import {FormControl, Validators} from '@angular/forms';
import {Topic} from '../model/Topic';
import {Event} from '../../event/models/Event';
import {Injectable} from '@angular/core';
import moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TopicValidationService {
  constructor() {}
  public isStatusValid(value: any): boolean {
    return new FormControl(value, [Validators.required])['status'] === 'INVALID';
  }
  public isDateOfTopicIncludeInEventRange(topicDate: Date, event: Event) {
    return (!moment(topicDate).set({'hour': 0, 'minutes': 0, 'seconds': 1}).isBetween(moment(event.dateStart).set({'hour': 0, 'minutes': 0, 'seconds': 0}).startOf('day'), moment(event.dateEnd).endOf('day')));
  }
  public isTitleNotEmpty(topic: Topic): boolean {
    return topic.title === undefined || topic.title.length === 0;
  }
}
