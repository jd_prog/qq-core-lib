import {Injectable} from '@angular/core';
import {Topic} from '../model/Topic';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class TopicEditService {

  private subject: Subject<Topic> = new Subject<Topic>();

  constructor() {}

  edit(topic: Topic) {
    this.subject.next(topic);
  }

  getEditedTopic(): Observable<Topic> {
    return this.subject.asObservable();
  }



}
