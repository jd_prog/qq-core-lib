import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {Stream} from '../../stream/model/stream';
import {Topic} from '../model/Topic';
import {Observable} from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class TopicService {

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {
  }

  getTopics(streamId: number) {
    return this.http.get<Topic[]>(`${this.urlProvider.getUrl()}api/streams/${streamId}/topics`);
  }

  findById(topicId: number): Observable<Topic> {
    return  this.http.get<Topic>(`${this.urlProvider.getUrl()}api/topics/${topicId}`);
  }


  async delete(topicId: number) {
    return await this.http.delete<Topic>(`${this.urlProvider.getUrl()}api/topics/${topicId}`)
      .toPromise();
  }


   update(streamId: number, topic: Topic) {
    return this.http.put<Topic>(`${this.urlProvider.getUrl()}api/streams/${streamId}/topics`, topic);
  }

  async create(streamId: number, topic: Topic) {
    return await this.http.post<Topic>(`${this.urlProvider.getUrl()}api/streams/${streamId}/topics`, topic)
      .toPromise();
  }





}
