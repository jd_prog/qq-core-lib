import {Pipe, PipeTransform} from '@angular/core';
import {Topic} from '../model/Topic';
import moment from 'moment';


@Pipe({
  name: 'uniqueHoursminutes',
  pure: false
})
export class SortTopicByMinutesAndHours implements PipeTransform {

  transform(array: Topic[]) {
    return array.sort( (a, b) => {
      return moment(a.startDate).unix() - moment(b.startDate).unix()});
  }

}
