import {Pipe, PipeTransform} from '@angular/core';
import {Topic} from '../model/Topic';
import moment from 'moment';

@Pipe({
  name: 'putToDayBucket',
  pure: false
})
export class SortTopicByDatesPipe implements PipeTransform {

  transform(value: Topic[], shortDate: Date): any {
    return value.filter( it => {
      if ( moment(it.startDate).isSame( moment(shortDate), 'day')    &&   moment(it.startDate).isSame( moment(shortDate), 'month')  ) {
        return it;
      }
    });
  }
}
