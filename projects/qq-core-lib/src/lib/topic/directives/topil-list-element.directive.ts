import {Directive, ElementRef, Input, Renderer2} from '@angular/core';


@Directive({
  selector: '[backGround]'

})
export class TopilListElementDirective {


  constructor(private elementRef: ElementRef, private render: Renderer2  ) {}


  @Input('backGround') set changeColor(topicActivity: string) {
    if (topicActivity === 'ACTIVE') {
      this.render.setStyle(this.elementRef.nativeElement, 'background-color', '#6391E6');
      this.render.setStyle(this.elementRef.nativeElement, 'color', '#E6E4D9');
    }
  }





}
