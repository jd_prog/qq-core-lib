import {Speaker} from '../../speaker/models/Speaker';
import * as firebase from "firebase";
import Timestamp = firebase.firestore.Timestamp;

export class Topic {
  public id: number;
  public description: string;
  public startDate: Date;
  public endDate: Date;
  public title: string;
  public users: Array<Speaker>;


  constructor () {}
}
