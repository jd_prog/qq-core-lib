import { NgModule } from '@angular/core';
import {TopilListElementDirective} from './directives/topil-list-element.directive';
import {SortTopicByDatesPipe} from './pipes/sort.topics.by.date.pipe';
import {SortTopicByMinutesAndHours} from './pipes/sort-topic-by-minutes-and-hours.pipe';

@NgModule({
  imports: [
  ],
  declarations: [
    TopilListElementDirective,
    SortTopicByDatesPipe,
    SortTopicByMinutesAndHours,

  ],
  exports: [
    TopilListElementDirective,
    SortTopicByDatesPipe,
    SortTopicByMinutesAndHours
  ]
})
export class QqTopicModule { }
