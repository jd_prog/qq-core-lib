import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {ConfirmDialogWindowComponent} from '../components/confirm-dialog-window/confirm-dialog-window';
import {SnackBarComponent} from "../components/snack-bar-component/snack.bar.component";

@Injectable({
  providedIn: 'root'
})
export class PopupsService {

  private confirmationDialogSubscription: Subscription;

  constructor(public dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  showConfirmationDialog(message: string,
                         positiveAnswer: string,
                         negativeAnswer: string,
                         onActionConfirmed: (confirmed: boolean) => void) {


    this.confirmationDialogSubscription =
      this.openDialogWindow(message, positiveAnswer, negativeAnswer)
        .afterClosed()
        .subscribe(confirm => {
          onActionConfirmed(confirm);
          this.confirmationDialogSubscription.unsubscribe();
        });
  }

  showSnackBar(message: string) {
    this.snackBar.openFromComponent(SnackBarComponent,{
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      data: {message: message},
      duration: 3000
    });
  }

  private openDialogWindow(message: string,
                           positiveAnswer: string,
                           negativeAnswer: string): MatDialogRef<ConfirmDialogWindowComponent> {

    return this.dialog.open(
      ConfirmDialogWindowComponent,
      this.dialogDataConfig(message, positiveAnswer, negativeAnswer));
  }

  private dialogDataConfig(message: string,
                           positiveAnswer: string,
                           negativeAnswer: string): any {
    return {
      data: {
        text: message, positiveAnswer: positiveAnswer, negativeAnswer: negativeAnswer
      }
    };
  }
}
