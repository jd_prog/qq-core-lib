import {Injectable} from '@angular/core';
import {QQRestConfig} from './q-q-rest.config';

/**
 * Created by yana on 20.04.18.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiUrlProvider {

  constructor() {
  }

  getUrl(): string {
    return QQRestConfig.BASE_URL;
  }


}
