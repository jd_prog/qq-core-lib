import { NgModule } from '@angular/core';
import {FilterByPredicatePipe} from './filter-by-predicate.pipe';

@NgModule({
  declarations: [
    FilterByPredicatePipe
  ],
  exports: [
    FilterByPredicatePipe
  ]
})
export class PipesModule { }
