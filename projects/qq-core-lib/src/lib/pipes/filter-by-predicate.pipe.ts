import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByPredicate',
  pure: false
})
export class FilterByPredicatePipe implements PipeTransform {

  transform(fields: any[], predicate: Function): any {
    return fields.filter((e) => predicate(e));
  }

}
