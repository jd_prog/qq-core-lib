import { FilterByPredicatePipe } from './filter-by-predicate.pipe';

describe('FilterByPredicatePipe', () => {

  it('filters array by predicate', () => {
    const fake_collection = [{id: 'take'}, {id: 'skip'}, {id: 'also_skip'}];

    function predecate(it: any): boolean {
      return it.id.includes('take');
    }

    const pipe = new FilterByPredicatePipe();
    expect(pipe).toBeTruthy();
    const filteredArray = pipe.transform(fake_collection, predecate);
    expect(filteredArray.length).toEqual(1);
    expect(filteredArray[0]).toEqual(fake_collection[0]);
  });

});
