import {Observable, throwError as observableThrowError} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import 'rxjs-compat/add/operator/concatMap';
import 'rxjs-compat/add/operator/catch';
import 'rxjs/add/observable/of';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
@Injectable()
export class ErrorForbiddenPageInterceptor implements HttpInterceptor {
  constructor(private router: Router) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(req)
      .do(() => {}).catch((e) => {
        if (e['status'] === 403) {
          this.router.navigate(['forbidden']);
        }
        return observableThrowError(e);
      });
  }
}

