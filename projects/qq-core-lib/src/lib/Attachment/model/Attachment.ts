export class Attachment {

  id: number;
  name: string;
  type: string;
  downloadUri: string;
  constructor() {}
}
