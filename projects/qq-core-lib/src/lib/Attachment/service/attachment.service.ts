import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {Observable} from 'rxjs';
import {Attachment} from '../model/Attachment';
import {Tag} from '../../tag/models/Tag';
import {text} from '@angular/core/src/render3/instructions';
import {Event} from '../../event/models/Event';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {
  }

  saveAttachment(attachment: File): Observable<Attachment> {
    const formdata: FormData = new FormData();
    formdata.append('attachment', attachment);
    return this.http.post<Attachment>(`${this.urlProvider.getUrl()}api/attachment/add`, formdata);
  }
  async deleteAttachment(attachmentId: number) {
    return await this.http.delete(`${this.urlProvider.getUrl()}api/attachment/${attachmentId}`, {}).toPromise();
  }
}
