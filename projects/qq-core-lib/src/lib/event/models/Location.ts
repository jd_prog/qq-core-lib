/**
 * Created by yana on 20.04.18.
 */
export class Location {

  public id: number;
  public latitude: number;
  public longitude: number;
  public formattedAddress: string;
  public cityName: string;
  public placeId: string;
}
