import {Location} from './Location';
import {Stream} from '../../stream/model/stream';
import {StatusEvent} from './StatusEvent';
import {User} from '../../security/models/User';
import {Tag} from '../../tag/models/Tag';
import {Attachment} from '../../Attachment/model/Attachment';

/**
 * Created by yana on 20.04.18.
 */
export class Event {

  id: number;
  title: string;
  description: string;
  location: Location;
  dateStart: Date;
  dateEnd: Date;
  streams: Array<Stream>;
  status: StatusEvent;
  participantsLimit: number;
  creator: User;
  tags: Array<Tag>;
  attachments: Array<Attachment> = [];

  constructor() {}
}
