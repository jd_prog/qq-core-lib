import {Pipe, PipeTransform} from '@angular/core';
import {Event} from '../models/Event';
import {StatusEvent} from '../models/StatusEvent';

/**
 * Created by vlad on 25.05.18.
 */
@Pipe({name: 'notPublishedEvents',
  pure: false})
export class NotPublished implements PipeTransform {
  statusEvent: StatusEvent.NOT_PUBLISH;
  transform(items: Event[]): Event[] {
    if (!items) { return []; }
    return items.filter(it => {
      return (it.status.toString() === 'NOT_PUBLISH');
    });
  }

}
