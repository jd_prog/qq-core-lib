import { Injectable, Pipe, PipeTransform } from '@angular/core';

/**
 * Created by yana on 26.04.18.
 */
@Pipe({
  name: 'sortByDate',
  pure: false})
@Injectable()
export class SortByDatePipe implements PipeTransform {
  transform(array: Array<any>, args: string): Array<any> {

    if (typeof args[0] === 'undefined') {
      return array;
    }
    let direction: string;
    direction = args[0][0];
    const column: string = args.replace('-', '');
    array.sort((item1: any, item2: any) => {
      const left = Number(item1[column]);
      const right = Number(item2[column]);
      return (direction === '-') ? right - left : left - right;
    });
    return array;
  }
}
