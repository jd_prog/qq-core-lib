import {Pipe, PipeTransform} from '@angular/core';
import {Event} from '../models/Event';

/**
 * Created by vlad on 23.04.18.
 */
@Pipe({name: 'nextTimeEvents',
  pure: false})
export class NextTimeEvents implements PipeTransform {
  transform(items: Event[]): Event[] {
    if (!items) { return []; }
    const currentDate = new Date();
    return items.filter(it => {
      return it.dateStart > currentDate;
    });
  }

}
