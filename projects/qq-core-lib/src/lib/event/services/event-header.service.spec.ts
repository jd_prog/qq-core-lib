import { TestBed } from '@angular/core/testing';

import { EventHeaderService } from './event-header.service';

describe('EventHeaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventHeaderService = TestBed.get(EventHeaderService);
    expect(service).toBeTruthy();
  });
});
