import { TestBed } from '@angular/core/testing';

import { SearchEventSubject } from './search-event.subject';

describe('SearchEventSubject', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchEventSubject = TestBed.get(SearchEventSubject);
    expect(service).toBeTruthy();
  });
});
