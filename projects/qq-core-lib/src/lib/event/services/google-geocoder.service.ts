import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

/**
 * Created by yana on 25.04.18.
 */

@Injectable({
  providedIn: 'root'
})
export class GoogleGeocoderService {

  private google_api_key = 'AIzaSyDSpY2u6qFKILrAsYTTSzfFhcz_xk3WQvY';
  private base_url = 'https://maps.googleapis.com/maps/api/';

  constructor(private http: HttpClient) {
  }

  findAddress(latitude: number, longitude: number): Observable<string> {

    return this.http
      .get(`${this.base_url}geocode/json?latlng=${latitude},${longitude}&key=${this.google_api_key}`, {withCredentials: false})
      .map(response => {
        return this.parseAddress(response);
      });
  }

  private parseAddress(json: any): string {
    return json['results'][0]['formatted_address'];
  }

  findCity(latitude: number, longitude: number) {
    return this.http
      .get(`${this.base_url}geocode/json?latlng=${latitude},${longitude}&key=${this.google_api_key}`);
  }

}
