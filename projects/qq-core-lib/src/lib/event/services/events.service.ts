/**
 * Created by yana on 20.04.18.
 */
import {Injectable} from '@angular/core';


import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Event} from '../models/Event';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {tap} from 'rxjs/operators';

import {UserDetailsService} from '../../security/services/user-details.service';
import 'rxjs-compat/add/operator/mergeMap';
import {User} from '../../security/models/User';
import {BaseBehaviorSubject} from './base-behaviour.subject';


@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private eventsContainer: BaseBehaviorSubject<Event> = new BaseBehaviorSubject<Event>();

  constructor(private http: HttpClient,
              private userDetailsService: UserDetailsService,
              private urlProvider: ApiUrlProvider) {
  }

  async findAll() {
    const currentEventList = await this.http.get<Array<Event>>(`${this.urlProvider.getUrl()}api/events`)
      .toPromise();

    if (currentEventList) {
      this.eventsContainer.replaceAll(currentEventList);
    }

    return currentEventList;
  }

  getEventsContainer(): BaseBehaviorSubject<Event> {
    return this.eventsContainer;
  }

  findAllCreatedByCurrentUser(): Observable<Array<Event>> {
    return this.userDetailsService.getCurrentUserUsername()
      .concatMap(username => this.http.get<Array<Event>>(`${this.urlProvider.getUrl()}api/users/${username}/events`))
      .pipe(tap(events => this.eventsContainer.replaceAll(events)));
  }

  findPublished(): Observable<Event[]> {

    return this.http.get<Array<Event>>(`${this.urlProvider.getUrl()}api/events/published`)
      .pipe(tap(events => this.eventsContainer.replaceAll(events)));
  }

  findById(id: number): Observable<Event> {
    return this.http.get<Event>(`${this.urlProvider.getUrl()}api/events/${id}`)
      .pipe(tap(event => this.eventsContainer.replace(event, (e1, e2) => e1.id == e2.id)));
  }

  createEmptyEvent() {

    return this.userDetailsService.getCurrentUserUsername()
      .map(currentUsername => {
        return {
          title: 'New event title',
          description: 'Description',
          dateStart: new Date(),
          dateEnd: new Date(),
          creator: {username: currentUsername}
        };
      })
      .flatMap(requestBody => this.http.post(`${this.urlProvider.getUrl()}api/events/`, requestBody));
  }

  async deleteEvent(event: Event) {
    await this.http.delete(`${this.urlProvider.getUrl()}api/events/${event.id}`)
      .toPromise()
      .then(() => {
        this.eventsContainer.remove(event, (e1, e2) => e1.id == e2.id);
      });
  }

  updateEvent(event: Event): Observable<Event> {
    return this.http.put<Event>(`${this.urlProvider.getUrl()}api/events`, event);
  }

  publishEvent(id): Observable<Event> {
    return this.http.post<Event>(`${this.urlProvider.getUrl()}api/events/${id}`, null);
  }

  createEvent(event: Event): Observable<Event> {
    return this.userDetailsService.getCurrentUserUsername()
      .map(currentUsername => {
        event.creator = new User();
        event.creator.username = currentUsername;
        return event;
      })
      .flatMap(eventRequestBody => this.http.post<Event>(`${this.urlProvider.getUrl()}api/events/`, eventRequestBody));
  }

  getQrCode(id: number): Observable<Blob> {
    return this.http.get(`${this.urlProvider.getUrl()}api/events/${id}/qrcode`, {responseType: 'blob'});
  }

  getEventByTagNameIn(text: string) {
    const filteredEventList =  this.http.post<Event[]>(`${this.urlProvider.getUrl()}api/events/filter/tags/`, text);
    if ( filteredEventList) {
      filteredEventList.subscribe(events => {
        this.eventsContainer.replaceAll(events);
      });
    }
    return filteredEventList;
  }
  getEventByTagName(tag: string) {
    const filteredEventList =  this.http.post<Event[]>(`${this.urlProvider.getUrl()}api/events/filter/tag/`, tag);
    if ( filteredEventList) {
      filteredEventList.subscribe(events => {
        this.eventsContainer.replaceAll(events);
      });
    }
    return filteredEventList;
  }

  addAttachmentToEvent(eventId: number, attachmentId: number): Observable<Event> {
    return this.http.put<Event>(`${this.urlProvider.getUrl()}api/events/${eventId}/attachments/${attachmentId}`, {});
  }
  async deleteAttachment(eventId: number, attachmentId: number) {
   return await this.http.delete<Event>(`${this.urlProvider.getUrl()}api/events/${eventId}/attachment/${attachmentId}`, {}).toPromise();
  }
}
