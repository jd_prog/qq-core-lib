import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Event} from '../models/Event';

@Injectable({
  providedIn: 'root'
})
export class EventHeaderService {
  subject = new  Subject< Event[] >();
  constructor() { }
  sendEvents(events: Event[]) {
  this.subject.next(events);
  }
}

