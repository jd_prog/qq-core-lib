import {inject, TestBed} from '@angular/core/testing';
import {EventNavigationSubjectService} from "./event-navigation-subject.service";
describe('EventNavigationSubjectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventNavigationSubjectService]
    });
  });

  it('should be created', inject([EventNavigationSubjectService], (service: EventNavigationSubjectService) => {
    expect(service).toBeTruthy();
  }));
});
