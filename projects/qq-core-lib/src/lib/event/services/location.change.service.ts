/// <reference types="@types/googlemaps" />
import {Location} from '../models/Location';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class LocationChangeService {

  public subject: Subject<Location> = new Subject<Location>();

  constructor() {}

  changeLocation(location: Location) {
    this.subject.next(location);
  }
}
