import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {User} from '../../security/models/User';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EventActualVisitorsService {
  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {}
  getAllActualVisitors(eventId: number): Observable<Array<User>> {
    return this.http.get<Array<User>>(`${this.urlProvider.getUrl()}api/events/${eventId}/actual/visitors`);
  }
  addEventActualVisitor(eventId: number, user: User): Observable<User> {
    return this.http.post<User>(`${this.urlProvider.getUrl()}api/events/${eventId}/actual/visitors/add`, user);
  }
}
