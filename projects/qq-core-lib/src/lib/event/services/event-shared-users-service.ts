import {Injectable} from '@angular/core';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {HttpClient} from '@angular/common/http';
import {Event} from '../models/Event';
import {User} from '../../security/models/User';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
  })
export class EventSharedUsersService {
              constructor(private http: HttpClient,
                          private urlProvider: ApiUrlProvider) {}
              getAllSharedUsers(eventId: number): Observable<Array<User>> {
                return this.http.get<Array<User>>(`${this.urlProvider.getUrl()}api/events/${eventId}/share`);
              }
              addAllSharedUsers(sharedUsers: Array<User>, eventId: number): Observable<Array<User>> {
                return this.http.post<Array<User>>(`${this.urlProvider.getUrl()}api/events/${eventId}/share/add`, sharedUsers);
              }
              deleteSharedUser(eventId: number, user: User): Observable<User> {
                return this.http.post<User>(`${this.urlProvider.getUrl()}api/events/${eventId}/share/remove`, user);
              }
              getAllEventShared(username: string): Observable<Array<Event>> {
                return this.http.get<Array<Event>>(`${this.urlProvider.getUrl()}api/${username}/shared`);
              }
}
