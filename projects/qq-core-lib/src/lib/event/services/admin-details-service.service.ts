import {Injectable} from '@angular/core';
import {Location} from '../models/Location';
import {Event} from '../models/Event';
import {Topic} from '../../topic/model/Topic';

@Injectable({
  providedIn: 'root'
})
export class AdminDetailsServiceService {

  constructor() {
  }

  assignLocation(event: Event) {
    if (event.location == null) {
      event.location = new Location();
      event.location.latitude = 0;
      event.location.longitude = 0;
    }
  }

  compareDates(startDate: Topic, endDate: Topic) {
    return startDate.startDate > endDate.startDate ? 1 : -1;
  }

}
