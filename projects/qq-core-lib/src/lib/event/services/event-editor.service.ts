import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {EventsService} from './events.service';
import {StreamService} from '../../stream/services/stream.service';
import {LocationChangeService} from './location.change.service';
import {AdminDetailsServiceService} from './admin-details-service.service';
import {Event} from '../models/Event';
import {Topic} from '../../topic/model/Topic';
import {FormControl, Validators} from '@angular/forms';
import moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class EventEditorService {

  private locationSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private eventsService: EventsService,
              private streamService: StreamService,
              private snackBar: MatSnackBar,
              private locationChangeSerive: LocationChangeService,
              private eventsDetailsAdminService: AdminDetailsServiceService) {
  }


  isEventValid(event: Event): boolean {
    return event.title && event.title !== null;
  }

  isValidParticipantLimit( event: Event): boolean {
    return (new FormControl(event.participantsLimit, [
      Validators.pattern('[0-9][0-9]{0,4}$'),
    ])['status'] === 'INVALID');
  }

  isValidDate(date: String): boolean {
    return date === '';
  }
  createEvent(event: Event): Observable<Event> {
    return this.eventsService.createEvent(event);
  }

  updateEvent(event: Event): Observable<Event> {

    return this.eventsService.updateEvent(event);
  }

  setDatesRangeToEvent(event: Event, dateStart: Date, dateEnd: Date) {
    event.dateEnd = dateEnd;
    event.dateStart = dateStart;
  }

  isEventDatesPassedToTopicsDates(dateStart: Date, dateEnd: Date, topics: Topic[]) {
    return (moment(dateEnd).unix() < moment(topics[topics.length - 1].endDate).unix()
      || moment(dateStart).unix() > moment(topics[0].startDate).unix());
  }

  parseEventTopics(event: Event): Topic[] {
    if (event.streams) {
      return event.streams
        .reduce((allTopics, stream) => allTopics.concat(stream.topics), [])
        .sort((f, n) => this.eventsDetailsAdminService.compareDates(f, n));
    } else { return []; }
  }

  requestLocationChanges(onLocationChanges: (location) => void) {
    if (this.locationChangeSerive) {
      this.locationSubscription = this.locationChangeSerive.subject.subscribe(location => {
        onLocationChanges(location);
      });
    }
  }

  unsubscribeAll() {
    this.unsubscribe(this.locationSubscription);
  }

  private unsubscribe(subscription: Subscription) {
    if (subscription) {
      subscription.unsubscribe();
    }
  }
}
