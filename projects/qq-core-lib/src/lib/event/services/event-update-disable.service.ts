import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EventUpdateDisableService {
  subject = new Subject<boolean>();
  disableEventUpdate(value: boolean) {
    this.subject.next(value);
  }
}
