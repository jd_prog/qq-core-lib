import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Event} from '../models/Event';


@Injectable({
  providedIn: 'root'
})
export class EventUpdateService {
  subject = new Subject<Event>();

  notifyEventChange(event: Event) {
    this.subject.next(event);
  }
}
