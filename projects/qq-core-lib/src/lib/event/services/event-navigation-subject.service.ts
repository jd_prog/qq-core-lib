import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventNavigationSubjectService extends Subject<{name: string, redirectEnabler: Subject<{allowRedirect: boolean, eventId: string}>}> {

  constructor() {
    super();
  }
}
