import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchEventSubject {
  private searchOnSubject = new Subject<boolean>();
  searchLifecycle: Observable<boolean> = this.searchOnSubject.asObservable();

  constructor() {
  }

  notify(show: boolean) {
    this.searchOnSubject.next(show);
  }
}
