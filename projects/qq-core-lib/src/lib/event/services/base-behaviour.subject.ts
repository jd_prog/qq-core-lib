import {BehaviorSubject, Subscription} from 'rxjs';

export class BaseBehaviorSubject<T> {

  subject: BehaviorSubject<T[]> = new BehaviorSubject([]);

  constructor() {}

  subscribe(next?: (value: T[]) => void): Subscription {
    return this.subject.subscribe(next);
  }

  private notifySubjectChanged(items: T[]) {
    this.subject.next(items);
  }

  replaceAll(items: T[]) {
    this.notifySubjectChanged(items);
  }

  add(item: T) {

    if (!item) {
      return;
    }

    const currentValue: T[] = this.subject.getValue();
    currentValue.push(item);
    this.subject.next(currentValue);
  }

  remove(item: T, equals: (e1: T, e2: T) => boolean) {
    const items: T[] = this.subject.getValue();

    const positionInTheList = items.findIndex((e) => equals(e, item));

    if (positionInTheList > -1) {
      items.splice(positionInTheList, 1);
    }

    this.notifySubjectChanged(items);
  }

  replace(item: T, equals: (e1: T, e2: T) => boolean) {

    if (!item) {
      return;
    }

    const items: T[] = this.subject.getValue();

    const positionInTheList = items.findIndex((e) => equals(e, item));

    if (positionInTheList > -1) {
      items[positionInTheList] = item;
    } else {
      items.push(item);
    }
    this.notifySubjectChanged(items);

  }
}
