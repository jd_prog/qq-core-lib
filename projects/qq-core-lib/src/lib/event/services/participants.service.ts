import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Visitor} from '../models/Visitor';
import {Observable} from 'rxjs/Observable';
import {ApiUrlProvider} from '../../services/api-url-provider';

@Injectable({
  providedIn: 'root'
})
export class ParticipantsService {

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {
  }
  formattedParticipantsStr(participantsCount: number, participantsLimit: number): string {
    return this.isParticipantsCountLimited(participantsLimit)
      ? `${participantsCount}/${participantsLimit}`
      : `${participantsCount}`;
  }

  placesNotAvailable(participantsCount: number, participantsLimit: number): boolean {
    return this.isParticipantsCountLimited(participantsLimit) && participantsCount >= participantsLimit;
  }

  isParticipantsCountLimited(participantsLimit: number): boolean {
    return participantsLimit > 0;
  }
}
