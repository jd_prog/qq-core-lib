import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminEventCreateDetailsService {
  subject = new Subject<string>();

  sendMessage(text: string) {
    this.subject.next(text);
  }
}
