import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PingBackEndService {
  constructor( private http: HttpClient,
               private urlProvider: ApiUrlProvider) {}

  startServer(): Observable<boolean> {
    return this.http.get<boolean>(`${this.urlProvider.getUrl()}api/server`);
  }

}
