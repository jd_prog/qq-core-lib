import {Location} from '../models/Location';
import {GoogleGeocoderService} from './google-geocoder.service';
import {LocationChangeService} from './location.change.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventMapService {

  constructor(private googleGeocoder:  GoogleGeocoderService,
              private changeLocationService: LocationChangeService) {}

  public getLocation(eventLocation: Location) {
    if ( eventLocation.latitude === 0 && eventLocation.longitude === 0) {
        navigator.geolocation.getCurrentPosition( postion => {
        eventLocation.latitude = postion.coords.latitude;
        eventLocation.longitude = postion.coords.longitude;
      });
    }
  }

  public changeMarker(event, eventLocation: Location) {
    eventLocation.latitude = event.coords.lat;
    eventLocation.longitude = event.coords.lng;
    this.googleGeocoder.findAddress(eventLocation.latitude, eventLocation.longitude).subscribe( value => {
      eventLocation.formattedAddress = value;
      this.changeLocationService.changeLocation(eventLocation);
    });
    this.googleGeocoder.findCity(eventLocation.latitude , eventLocation.longitude).subscribe( mapResponse => {
      eventLocation.placeId = mapResponse['results'][0].place_id;
      console.log(eventLocation.placeId);
      console.log(mapResponse['results'][0].place_id);
      mapResponse['results'][0].address_components.forEach( (it) => {
        if (it.types[0] === 'locality' && it.types[1] === 'political') {
          eventLocation.cityName = it.short_name;
        }
      });
    });
  }
}
