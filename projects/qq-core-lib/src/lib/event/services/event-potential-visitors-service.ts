import {Injectable} from '@angular/core';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {HttpClient} from '@angular/common/http';
import {User} from '../../security/models/User';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventPotentialVisitorsService {
  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {}
  getAllPotentialVisitors(eventId: number): Observable<Array<User>> {
    return this.http.get<Array<User>>(`${this.urlProvider.getUrl()}api/events/${eventId}/potential/visitors`);
  }
  addEventPotentialVisitor(eventId: number, user: User): Observable<User> {
    return this.http.post<User>(`${this.urlProvider.getUrl()}api/events/${eventId}/potential/visitors/add`, user);
  }
  removeEventPotentialVisitor(eventId: number, user: User): Observable<User> {
    return this.http.post<User>(`${this.urlProvider.getUrl()}api/events/${eventId}/potential/visitors/remove`, user);
  }
}
