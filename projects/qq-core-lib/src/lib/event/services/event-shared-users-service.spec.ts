import {async, inject, TestBed} from '@angular/core/testing';
import {EventSharedUsersService} from './event-shared-users-service';
import {User} from '../../security/models/User';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/of';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {Event} from '../models/Event';
import {StatusEvent} from '../models/StatusEvent';
import {BrowserStack} from 'protractor/built/driverProviders';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

class MockHttpClient {
  get() {}
  post() {}
}
class MockApiUrlProvider {
  getUrl() {}
}

describe('EventSharedUsersServiceService', () => {
  let service: EventSharedUsersService;
  let http: HttpClient;
  let urlProvide: ApiUrlProvider;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FlexLayoutModule, BrowserTestingModule, HttpClientTestingModule],
      providers: [EventSharedUsersService,
        {provide: HttpClient, useClass: MockHttpClient},
        {provide: ApiUrlProvider, useClass: MockApiUrlProvider}]
    });
    service = TestBed.get(EventSharedUsersService);
    urlProvide = TestBed.get(ApiUrlProvider);
    http = TestBed.get(HttpClient);
  }));

  it('should return all shared users', () => {
    const fakeId = 7;
    const fakeSharedUsersList: Array<User> = [
      {username: 'John'},
      {username: 'George'}
    ];
    spyOn(http, 'get').and.returnValues(Observable.of(fakeSharedUsersList));
    spyOn(urlProvide, 'getUrl');
    service.getAllSharedUsers(fakeId).subscribe(users => {
      expect(users).toEqual(fakeSharedUsersList);
    });
    expect(http.get).toHaveBeenCalled();
    expect(urlProvide.getUrl).toHaveBeenCalled();
  });
  it('should return all event of user', () => {

    const fakeCreator: User = {
      username: 'maxnelipa',
    };
    const fakeUser: User = {
      username: 'gorgio'
    };
    const fakeEventsList: Array<Event> = [
      {
        id: 3,
        title: 'Java',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 7,
        title: 'TypeScript',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 9,
        title: 'JavaScript',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 4,
        title: 'Kafka',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 1,
        title: 'SQL',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      }
    ];
    spyOn(http, 'get').and.returnValues(Observable.of(fakeEventsList));
    spyOn(urlProvide, 'getUrl');
    service.getAllEventShared(fakeUser.username).subscribe(events => {
      expect(events).toEqual(fakeEventsList);
    });
    expect(http.get).toHaveBeenCalled();
    expect(urlProvide.getUrl).toHaveBeenCalled();
  });
  it('should call add method', () => {
    const fakeId = 7;
    const fakeSharedUsersList: Array<User> = [
      {username: 'John'},
      {username: 'George'}
    ];
    spyOn(http, 'post').and.returnValues(Observable.of(fakeSharedUsersList));
    spyOn(urlProvide, 'getUrl');
    service.addAllSharedUsers(fakeSharedUsersList, fakeId).subscribe(sharedUsers => {
      expect(sharedUsers).toEqual(fakeSharedUsersList);
    });
  });
});
