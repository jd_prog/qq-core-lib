import { Injectable } from '@angular/core';
import {Event} from '../models/Event';
import  moment from 'moment';

@Injectable({
  providedIn: 'root'
})export class EventStatusService {

  constructor() { }

  upComing(event: Event) {
    return moment(event.dateStart).isAfter(moment());
  }

  notPassedEvents(events: Event) {
    return ! this.passed(events);
  }

  lasting(event: Event) {
    return moment().isBetween(event.dateStart, event.dateEnd);
  }

  passed(event: Event) {
    return moment(event.dateEnd).isBefore(moment());
  }

  afterNextMonth(event: Event) {
    const nextNextMonth = moment().add(2, 'month');
    return moment(event.dateStart).isAfter(nextNextMonth);
  }

  onNextMonth(event: Event) {
    const nextNextMonth = moment().add(2, 'month');
    const nextMonth = moment().add(1, 'month');
    return moment(event.dateStart).isBetween(nextMonth, nextNextMonth);
  }

  onThisMonth(event: Event) {
    const nextMonth = moment().add(1, 'month');
    return moment(event.dateStart).isBetween(moment(), nextMonth);
  }

}
