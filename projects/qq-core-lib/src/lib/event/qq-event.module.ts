import { NgModule } from '@angular/core';
import {NextTimeEvents} from './pipes/next-time-events';
import {NotPublished} from './pipes/not-published';
import {PublishedEvents} from './pipes/published-events';
import {SortByDatePipe} from './pipes/sort-by-date.pipe';
import {DirectiveModule} from './directives/directive.module';

@NgModule({
  declarations: [
    NextTimeEvents,
    NotPublished,
    PublishedEvents,
    SortByDatePipe
  ],
  exports: [
    NextTimeEvents,
    NotPublished,
    PublishedEvents,
    SortByDatePipe
  ],
  imports: [
    DirectiveModule
  ]
})
export class QQEventModule { }
