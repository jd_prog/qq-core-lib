import {NgModule} from '@angular/core';
import {DisableDirective} from './disable.directive';



@NgModule({
  declarations: [DisableDirective]
})
export class DirectiveModule {}
