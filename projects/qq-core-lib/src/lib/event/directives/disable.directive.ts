import {Directive, ElementRef, HostListener, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';


@Directive({
  selector: '[qslDisable]'
})
export class DisableDirective {
  @ViewChild('mat-button') elementRef: ElementRef;

  @HostListener('click') disableClick() {
    console.log(this.elementRef.nativeElement);
  }

}
