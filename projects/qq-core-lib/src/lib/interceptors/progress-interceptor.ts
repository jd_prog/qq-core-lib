import {Observable, throwError as observableThrowError} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import 'rxjs-compat/add/operator/concatMap';
import 'rxjs-compat/add/operator/catch';
import 'rxjs/add/observable/of';
import {SaveRequestNotificator} from '../progress-loader/service/save-request-notificator.service';

@Injectable()
export class ProgressInterceptor implements HttpInterceptor {

  constructor(private loader: SaveRequestNotificator) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const condition = this.conditionSpining(req);
    if (condition) {
      this.showSaveIndicator();
     }
       console.log();
      return next
        .handle(req)
        .do(event => {
          if (event instanceof HttpResponse && condition) {
              this.hideSaveIndicator();
          }
        }).catch((e) => {
          this.errorMessage();
          return observableThrowError(e);
        });
    }
    conditionSpining(req: HttpRequest<any>): boolean {
      return (req.method === 'POST' || req.method === 'DELETE' ||  req.method === 'PUT');
    }

  showSaveIndicator() {
    this.loader.notify(true);
  }
  hideSaveIndicator() {
    this.loader.notify(false);
  }
  errorMessage() {
    this.loader.notify(null);
  }
}

