import { TestBed } from '@angular/core/testing';

import { TagService } from './tag.service';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ApiUrlProvider} from '../../services/api-url-provider';


class MockApiUrlProvider {
  getUrl() {}
}


describe('TagService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ BrowserTestingModule, HttpClientTestingModule],
    providers: [
      {provide: ApiUrlProvider, useClass: MockApiUrlProvider}]
  }));

  it('should be created', () => {
    const service: TagService = TestBed.get(TagService);
    expect(service).toBeTruthy();
  });
});
