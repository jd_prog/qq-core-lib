import { Injectable } from '@angular/core';
import {Tag} from '../models/Tag';
import {ApiUrlProvider} from '../../services/api-url-provider';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) { }

  create(tag: Tag): Observable<Tag> {
    return this.http.post<Tag>(`${this.urlProvider.getUrl()}api/tags`, tag);
  }
  addTagToEvent(eventId: string, tagId: string): Observable<Tag> {
    return this.http.put<Tag>(`${this.urlProvider.getUrl()}api/events/${eventId}/tags/${tagId}`, {});
  }
  matTagToAnotherTag(rootTagId: string, tagId: string): Observable<Tag> {
    return this.http.put<Tag>(`${this.urlProvider.getUrl()}api/tags/${rootTagId}/${tagId}`, {});
  }
  removeFromEvent(eventId: string, tagId: string) {
    return this.http.delete(`${this.urlProvider.getUrl()}api/events/${eventId}/tags/${tagId}`);
  }
}
