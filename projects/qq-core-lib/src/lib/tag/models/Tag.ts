

export class Tag {
  id: number;
  name: string;
  tagRelations: Tag[];
}
