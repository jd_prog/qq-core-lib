/**
 * Created by yana on 25.04.18.
 */
import {Injectable} from '@angular/core';


import {HttpClient} from '@angular/common/http';
import {Speaker} from '../models/Speaker';
import {Observable} from 'rxjs';
import {ApiUrlProvider} from '../../services/api-url-provider';


@Injectable({
  providedIn: 'root'
})
export class SpeakersService {

  constructor(private http: HttpClient,
              private urlProvider: ApiUrlProvider) {
  }

  findAllByEvent(eventId: number): Observable<Array<Speaker>> {
    return this.http.get<Array<Speaker>>(`${this.urlProvider.getUrl()}users/events/${eventId}/speakers`);
  }

  findAllByTopic(topicId: number): Observable<Array<Speaker>> {
    return this.http.get<Array<Speaker>>(`${this.urlProvider.getUrl()}users/topics/${topicId}/speakers`);
  }
  findAll(): Observable<Array<Speaker>> {
    return this.http.get<Array<Speaker>>(`${this.urlProvider.getUrl()}users/public`);
  }

  findAllEventUsers(eventId: number): Observable<Array<Speaker>> {
    return this.http.get<Array<Speaker>>(`${this.urlProvider.getUrl()}users/events/${eventId}/all`);
  }


}
