import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserJoinNotificationService {
  subject = new Subject<string>();


  join() {
    this.subject.next('join');
  }

  leave() {
    this.subject.next('leave');
  }
}
