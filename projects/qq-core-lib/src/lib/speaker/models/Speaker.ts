/**
 * Created by yana on 25.04.18.
 */

export class Speaker {
  username: string;
  email: string;
  displayName: string;
  image: string;
}
