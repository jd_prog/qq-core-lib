import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Oauth2Service} from '../services/oauth2.service';
import {Observable} from 'rxjs/Observable';
import {AuthService} from 'angular5-social-login';

@Injectable({
  providedIn: 'root'
})
export class LoginPageGuard implements CanActivate {

  constructor(private authService: Oauth2Service,
              private socialAuthService: AuthService,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.isAuthenticated()
      .do(authenticated => {
        if (authenticated) {
          this.router.navigate(next.data.destinationRoute);
        }
      })
      .map(authentificated => !authentificated );
  }
}
