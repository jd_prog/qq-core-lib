import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute} from '@angular/router';
import {Oauth2Service} from '../services/oauth2.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs-compat/add/operator/do';
import {AuthService} from 'angular5-social-login';
import {StreamService} from '../../stream/services/stream.service';

@Injectable({
  providedIn: 'root'
})
export class ClientAuthGuard implements CanActivate {

  constructor(private authService: Oauth2Service,
              private socialAuthService: AuthService,
              private streamService: StreamService,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.isAuthenticated()
      .do(authenticated => {
        if (!authenticated) {
          this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
        }
      });
  }
}
