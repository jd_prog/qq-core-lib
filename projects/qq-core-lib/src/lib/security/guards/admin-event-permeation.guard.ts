import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs-compat/add/operator/map';
import 'rxjs/add/observable/zip';
import 'rxjs/add/operator/do';
import {UserDetailsService} from '../services/user-details.service';
import {EventsService} from '../../event/services/events.service';
import {Event} from '../../event/models/Event';
import {EventSharedUsersService} from '../../event/services/event-shared-users-service';
import {User} from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AdminEventPermeationGuard implements CanActivate {

  constructor(private userDetailsService: UserDetailsService,
              private activatedRoute: ActivatedRoute,
              private eventsService: EventsService,
              private eventSharedUserService: EventSharedUsersService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
   return this.userDetailsService.getCurrentUserUsername()
     .flatMap(username => {
       return Observable.zip(
         this.eventSharedUserService.getAllSharedUsers(route.params.id),
         this.eventsService.findById(route.params.id),
         (sharedUsers: Array<User>, event: Event) => ({sharedUsers, event})
       ).map(param => {
         return (param.event.creator.username === username) || (param.sharedUsers.some(user => user.username === username));
       }).do(allowRedirect => {
         if (!allowRedirect) {
           this.redirectToPermissionDenied();
         }
       });
     });
  }

  private redirectToPermissionDenied() {
    this.router.navigate(['forbidden']);
  }
}
