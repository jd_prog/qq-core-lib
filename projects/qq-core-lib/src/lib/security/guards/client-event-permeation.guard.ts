import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs-compat/add/operator/map';
import 'rxjs/add/observable/zip';
import 'rxjs/add/operator/do';
import {UserDetailsService} from '../services/user-details.service';
import {EventsService} from '../../event/services/events.service';
import {Event} from '../../event/models/Event';
import {StatusEvent} from '../../event/models/StatusEvent';

@Injectable({
  providedIn: 'root'
})
export class ClientEventPermeationGuard implements CanActivate {

  constructor(private userDetailsService: UserDetailsService,
              private activatedRoute: ActivatedRoute,
              private eventsService: EventsService,
              private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.eventsService.findById(route.params.eventId)
      .map(event => {
        return   (event.status.toString() === StatusEvent.PUBLISH.toString()); })
      .do(allowRedirect => {
        if (!allowRedirect) {
          this.redirectToPermissionDenied();
        }
      });
  }
  private redirectToPermissionDenied() {
    this.router.navigate(['forbidden']);
  }
}
