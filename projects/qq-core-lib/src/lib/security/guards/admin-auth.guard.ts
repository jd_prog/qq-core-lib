import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs-compat/add/operator/map';
import {UserDetailsService} from '../services/user-details.service';
import {Oauth2Service} from '../services/oauth2.service';

import {StreamService} from '../../stream/services/stream.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {

  constructor(private userDetailsService: UserDetailsService,
              private oauthService: Oauth2Service,
              private streamService: StreamService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.userDetailsService.getCurrentUserRole()
      .map(userRole => {

        if (!userRole || userRole !== route.data.expectedRole) {
          this.redirectToLogin();
          return false;
        }

        return true;
      });
  }

  private redirectToLogin() {
    this.router.navigate(['login']);
  }

}
