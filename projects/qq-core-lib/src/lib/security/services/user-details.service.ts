import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {LocalStorage} from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class UserDetailsService {

  constructor(private localStorage: LocalStorage) { }

  getCurrentUserDisplayName(): Observable<string> {

    return this.localStorage.getItem('display_name');
  }

  getCurrentUserUsername(): Observable<string> {
    return this.localStorage.getItem('username');
  }

  getCurrentUserAccessToken(): Observable<string> {

    return this.localStorage.getItem('access_token');
  }

  getCurrentUserRefreshToken(): Observable<string> {

    return this.localStorage.getItem('refresh_token');
  }

  getCurrentUserRole(): Observable<string> {

    return this.localStorage.getItem('role');
  }

  setCurrentUserDisplayName(displayName: string): Observable<boolean> {

    return this.localStorage.setItem('display_name', displayName);
  }

  setCurrentUserUsername(username: string): Observable<boolean> {

    return this.localStorage.setItem('username', username);
  }

  setCurrentUserAccessToken(accessToken: string): Observable<boolean> {

    return this.localStorage.setItem('access_token', accessToken);
  }

  setCurrentUserRefreshToken(refreshToken: string): Observable<boolean> {

    return this.localStorage.setItem('refresh_token', refreshToken);
  }

  setCurrentUserRole(role: string): Observable<boolean> {

    return this.localStorage.setItem('role', role);
  }

  clearCurrentUserDetails(): Observable<boolean> {

    return this.localStorage.removeItem('display_name')
      .concatMap(() => this.localStorage.removeItem('username'))
      .concatMap(() => this.localStorage.removeItem('access_token'))
      .concatMap(() => this.localStorage.removeItem('refresh_token'))
      .concatMap(() => this.localStorage.removeItem('role'));
  }
}
